package com.theuntitled.alarmclock.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.theuntitled.alarmclock.Activities.EditAlarm;
import com.theuntitled.alarmclock.Activities.MainActivity;
import com.theuntitled.alarmclock.Database.Databaseconnection;
import com.theuntitled.alarmclock.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

/**
 * Created by Fizzz on 5/20/2017.
 */

public class Adaptermainactivity extends BaseAdapter {
    MainActivity context;
    ArrayList<String> ids = new ArrayList<>();
    ArrayList<String> nms = new ArrayList<>();
    ArrayList<String> dts = new ArrayList<>();
    ArrayList<String> tms = new ArrayList<>();
    ArrayList<String> sts = new ArrayList<>();
    ArrayList<String> reqcodes = new ArrayList<>();
    LayoutInflater layoutInflater;
    Holder holder;
    Databaseconnection obj = new Databaseconnection();
    public static String selection = "";
    Random r;
    int random;
    int a;
    public Adaptermainactivity(MainActivity mainActivity, ArrayList<String> ids, ArrayList<String> nms, ArrayList<String> dts, ArrayList<String> tms, ArrayList<String> sts, ArrayList<String> reqcodes) {
        context = mainActivity;
        this.ids = ids;
        this.nms = nms;
        this.dts = dts;
        this.tms = tms;
        this.sts = sts;
        this.reqcodes = reqcodes;
    }

    @Override
    public int getCount() {
        return ids.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.adaptermainactivity,null);
        holder = new Holder();
        holder.dt = (TextView)view.findViewById(R.id.textdate);
        holder.tm = (TextView)view.findViewById(R.id.texttime);
        holder.nm = (TextView)view.findViewById(R.id.textView2);
        holder.s = view.findViewById(R.id.switch1);
        holder.ll = (RelativeLayout)view.findViewById(R.id.layoutll);
        r = new Random();
        random = r.nextInt(11-1)+1;
        if ((i % 6) == 0){
            holder.ll.setBackgroundColor(ContextCompat.getColor(context,R.color.shade2));
            //holder.ll.setBackground(ContextCompat.getDrawable(context,R.drawable.gradient1));
        } else if ((i % 6) == 1){
            holder.ll.setBackgroundColor(ContextCompat.getColor(context,R.color.shade3));
            //holder.ll.setBackground(ContextCompat.getDrawable(context,R.drawable.gradient2));
        } else if ((i % 6) == 2){
            holder.ll.setBackgroundColor(ContextCompat.getColor(context,R.color.shade4));
            //holder.ll.setBackground(ContextCompat.getDrawable(context,R.drawable.gradient3));
        } else if ((i % 6) == 3){
            holder.ll.setBackgroundColor(ContextCompat.getColor(context,R.color.shade5));
            //holder.ll.setBackground(ContextCompat.getDrawable(context,R.drawable.gradient4));
        } else if ((i % 6) == 4){
            holder.ll.setBackgroundColor(ContextCompat.getColor(context,R.color.shade6));
            //holder.ll.setBackground(ContextCompat.getDrawable(context,R.drawable.gradient5));
        } else if ((i % 6) == 5){
            holder.ll.setBackgroundColor(ContextCompat.getColor(context,R.color.shade7));
            //holder.ll.setBackground(ContextCompat.getDrawable(context,R.drawable.gradient5));
        }

        if (sts.get(i).equals("off")){
            try {
                SimpleDateFormat parser = new SimpleDateFormat("h:mm a", Locale.US);
                Log.d("SSSdate",tms.get(i) + " == " +parser.parse(tms.get(i)) );
                if (parser.parse(tms.get(i)).before(new Date())){

                    if (new SimpleDateFormat("dd/MM/yyyy").parse(dts.get(i)).before(new Date())) {

                        SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
                        Calendar c1 = Calendar.getInstance();
                        c1.setTime(new Date());
                        c1.add(Calendar.DATE,1);// number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE

                        String output1 = sdf1.format(c1.getTime());
                        holder.dt.setText(output1);
                        obj.updatedateandtime(context,output1,ids.get(i));
                    }
                    else {
                        holder.dt.setText(dts.get(i));
                    }
                }
//                if (new SimpleDateFormat("dd/MM/yyyy").parse(dts.get(i)).before(new Date())) {}

            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            holder.dt.setText(dts.get(i));
        }
        holder.tm.setText(tms.get(i));
        holder.nm.setText(nms.get(i));
        if (sts.get(i).equals("on")){
            holder.s.setChecked(true);
        }else {
            holder.s.setChecked(false);
        }
        view.setTag(ids.get(i));

        final View finalView = view;
        holder.s.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    obj.updatealarmonoff(context, finalView.getTag().toString(),"on");
                } else {
                    obj.updatealarmonoff(context, finalView.getTag().toString(),"off");
                }
            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selection = view.getTag().toString();
                context.startActivity(new Intent(context, EditAlarm.class));

            }
        });

        return view;
    }

    class Holder{
        TextView dt,nm,tm;
        SwitchCompat s;
        RelativeLayout ll;
        int a;
    }
}
