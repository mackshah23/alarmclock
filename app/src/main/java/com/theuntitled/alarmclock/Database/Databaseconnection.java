package com.theuntitled.alarmclock.Database;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.theuntitled.alarmclock.Services.AlarmReceiver;

import java.util.ArrayList;
import java.util.Calendar;

import static android.content.Context.ALARM_SERVICE;

/**
 * Created by Fizzz on 5/20/2017.
 */

public class Databaseconnection {

    SQLiteDatabase db;
    String DATABASENAME = "alarmclock.db";
    AlarmManager alarmManager;
    private PendingIntent pendingIntent;
    String t1,d1 = "";
    public void createdatabase(Context context)
    {
        db = context.openOrCreateDatabase(DATABASENAME,context.MODE_PRIVATE,null);
        db.execSQL("create table if not exists allalarms(id integer primary key AUTOINCREMENT,name text)");
        db.execSQL("create table if not exists alarmdetails(id integer primary key AUTOINCREMENT, name text,alarmdate text,alarmtime text,ringtone text,snooze text,voice text,repeat text,onoff text,reqcode int)");
        db.execSQL("create table if not exists defaultvals(id integer primary key AUTOINCREMENT,rington text,voic text, ringtoneuri text, voiceuri text)");
        db.close();
    }

    public void insertallalarmdetails(Context context, String a, String b, String c , String d, String e, String f, String g, String h, int code)
    {
        db = context.openOrCreateDatabase(DATABASENAME,context.MODE_PRIVATE,null);
        db.execSQL("insert into alarmdetails(name,alarmdate,alarmtime,ringtone,snooze,voice,repeat,onoff,reqcode) values ('"+ a +"','"+b+"','"+ c +"','"+ d +"','"+ e +"','"+f+"','"+g+"','"+h+"', "+ code +") ");
        db.close();
    }

    public void insertvals(Context context, String a, String b, String c, String d)
    {
        db = context.openOrCreateDatabase(DATABASENAME,context.MODE_PRIVATE,null);
        db.execSQL("insert into defaultvals(rington,voic,ringtoneuri,voiceuri) values ('"+ a +"','"+b+"','"+ c +"','"+d+"') ");
        db.close();
    }

    public void updatevals(Context context, String a, String b, String c, String d)
    {
        db = context.openOrCreateDatabase(DATABASENAME,context.MODE_PRIVATE,null);
        db.execSQL("update defaultvals set rington = '"+ a +"',voic = '"+b+"',ringtoneuri = '"+ c +"',voiceuri = '"+d+"' ");
        db.close();
    }

    public void updatealarmonoff(Context context, String a, String b)
    {
        db = context.openOrCreateDatabase(DATABASENAME,context.MODE_PRIVATE,null);
        String query = "select alarmdate,alarmtime,reqcode from alarmdetails where id = '" + a + "'";
        String reqcd = "";
        Cursor c1 = db.rawQuery(query,null);
        while (c1.moveToNext()){
            d1 = c1.getString(0);
            t1 = c1.getString(1);
            reqcd = c1.getString(2);
        }
        Log.d("aaaa",d1 + " -- " + t1);

        c1.close();
        String[] seldate = d1.split("/");
        String[] tme = t1.split(":");
        String a1 = tme[0];
        String b1 = tme[1];
        String[] secs = b1.split(" ");
        String sc = secs[0];
        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.DATE, Integer.parseInt(seldate[0]));
        cal.set(Calendar.MONTH, Integer.parseInt(seldate[1]) - 1);
        cal.set(Calendar.YEAR, Integer.parseInt(seldate[2]));

        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(a1));
        cal.set(Calendar.MINUTE, Integer.parseInt(sc));
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND,0);

        Log.d("aaaa",a + " " + b);
        Intent myIntent = new Intent(context, AlarmReceiver.class);
        myIntent.putExtra("code", reqcd + "");
        pendingIntent = PendingIntent.getBroadcast(context, Integer.parseInt(reqcd), myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
        if (b.equalsIgnoreCase("on")) {
            Intent myInten = new Intent(context, AlarmReceiver.class);
            pendingIntent = PendingIntent.getBroadcast(context, Integer.parseInt(reqcd),myInten,0);
            myIntent.putExtra("code", reqcd + "");
            alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
            db.execSQL("update alarmdetails set onoff = '" + b + "' where id = '" + a + "'");
        }else {

            db.execSQL("update alarmdetails set onoff = '" + b + "' where id = '" + a + "'");
        }
        db.close();
    }

    public void updateallalarmdetails(Context context, String a, String b, String c, String d, String e, String g, String h, String i)
    {
        db = context.openOrCreateDatabase(DATABASENAME,context.MODE_PRIVATE,null);

        db.execSQL("update alarmdetails set name = '"+b+"', alarmdate='"+c+"', alarmtime='"+d+"', ringtone='"+e+"', voice='"+g+"', repeat='"+h+"',onoff='"+i+"' where id = '"+a+"'");
        Log.d("XXXquery","update alarmdetails set name = '"+b+"', alarmdate='"+c+"', alarmtime='"+d+"', ringtone='"+e+"', voice='"+g+"', repeat='"+h+"',onoff='"+i+"' where id = '"+a+"'");
        db.close();
    }


    public void updatedateandtime(Context context, String dat, String idd)
    {
        db = context.openOrCreateDatabase(DATABASENAME,context.MODE_PRIVATE,null);

        db.execSQL("update alarmdetails set alarmdate='"+dat+"' where id = '"+idd+"'");
        db.close();
    }

    public void turnoffalarm(Context context, String reqcode)
    {
        db = context.openOrCreateDatabase(DATABASENAME,context.MODE_PRIVATE,null);
        db.execSQL("update alarmdetails set onoff='off' where reqcode = '"+reqcode+"'");
        db.close();
    }

    public void deletedata(Context context, String a)
    {
        db = context.openOrCreateDatabase(DATABASENAME,context.MODE_PRIVATE,null);

        String query = "select reqcode from alarmdetails where id = '" + a + "'";
        String req = "";
        Cursor c1 = db.rawQuery(query,null);
        while (c1.moveToNext()){
            req = c1.getString(0);
        }
        c1.close();

        Intent myIntent = new Intent(context, AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(context, Integer.parseInt(req), myIntent, 0);
        alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);


        db.execSQL("delete from alarmdetails where id='" + a + "' ");
        db.close();
    }

    public ArrayList<String> alarmdetails(Context context, String id)
    {
        ArrayList<String> items= new ArrayList<>();
        db= context.openOrCreateDatabase(DATABASENAME,context.MODE_PRIVATE,null);
        Cursor c1 = db.rawQuery("select * from alarmdetails where id = '"+ id +"'",null);
        while(c1.moveToNext())
        {
           // String ids = c1.getString(0);
            String idd =c1.getString(0);
            String nm =c1.getString(1);
            String dt = c1.getString(2);
            String tm = c1.getString(3);
            String ring = c1.getString(4);
            String snooz = c1.getString(5);
            String vce = c1.getString(6);
            String rpt = c1.getString(7);
            String stats = c1.getString(8);
            String codes = c1.getString(9);
            Log.d("AAA", " " + idd + " " + nm + " " + dt + " " + tm + " " + ring + " " + snooz + " " + vce + " " + rpt + " " + stats);

            items.add(idd);
            items.add(nm);
            items.add(dt);
            items.add(tm);
            items.add(ring);
            items.add(snooz);
            items.add(vce);
            items.add(rpt);
            items.add(stats);
            items.add(codes);

        }
        c1.close();
        db.close();
        return items;
    }

    public ArrayList<String> alarmfewdetails(Context context)
    {
        ArrayList<String> items= new ArrayList<>();
        db= context.openOrCreateDatabase(DATABASENAME,context.MODE_PRIVATE,null);
        Cursor c1 = db.rawQuery("select id,name,alarmdate,alarmtime,onoff,reqcode from alarmdetails"  ,null);
        while(c1.moveToNext())
        {
            String id = c1.getString(0);
            String nm = c1.getString(1);
            String dt = c1.getString(2);
            String tm = c1.getString(3);
            String sts = c1.getString(4);
            String codes = c1.getString(5);
            items.add(id);
            items.add(nm);
            items.add(dt);
            items.add(tm);
            items.add(sts);
            items.add(codes);
        }
        c1.close();
        db.close();
        return items;
    }

    public ArrayList<String> alarmsongs(Context context, String reqcode){
        ArrayList<String> items= new ArrayList<>();
        db= context.openOrCreateDatabase(DATABASENAME,context.MODE_PRIVATE,null);
        Cursor c1 = db.rawQuery("select ringtone,voice,name,alarmtime from alarmdetails where reqcode = '" + reqcode + "'"  ,null);
        while(c1.moveToNext())
        {
            items.add(c1.getString(0));
            items.add(c1.getString(1));
            items.add(c1.getString(2));
            items.add(c1.getString(3));
        }
        c1.close();
        db.close();
        return items;
    }

    public ArrayList<String> getdefaults(Context context){
        ArrayList<String> items= new ArrayList<>();
        db= context.openOrCreateDatabase(DATABASENAME,context.MODE_PRIVATE,null);
        Cursor c1 = db.rawQuery("select * from defaultvals",null);
        while(c1.moveToNext())
        {
            items.add(c1.getString(0));
            items.add(c1.getString(1));
            items.add(c1.getString(2));
            items.add(c1.getString(3));
            items.add(c1.getString(4));
        }
        c1.close();
        db.close();
        return items;
    }

}
