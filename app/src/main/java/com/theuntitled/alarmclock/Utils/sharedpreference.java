package com.theuntitled.alarmclock.Utils;

/**
 * Created by soham on 6/24/2017.
 */

import android.content.Context;
import android.content.SharedPreferences;
/**
 * Created by Soham on 5/5/2016.
 */
public class sharedpreference {

    public static final String MyPREFERENCES = "AlarmClock_TUT" ;

    public static String reqcode="0";
    public static String token="token";
    public static String userid="UserID";
    public static String promo="Promo";
    public static String devid="DevID";
    public static String firstinstall="NO";
    public static String firebaseid = "FirebaseID";

    public static int getReqCode(Context c1) {
        SharedPreferences sharedpreferences = c1.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        int ans =sharedpreferences.getInt(reqcode,0);
        return  ans;
    }
    public static void setReqCode(Context c1,int value) {
        SharedPreferences sharedpreferences = c1.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt(reqcode, value);
        editor.apply();
    }

    public static String gettoken(Context c1) {
        SharedPreferences sharedpreferences = c1.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String ans =sharedpreferences.getString(token,"no");
        return  ans;
    }
    public static void settoken(Context c1,String value) {
        SharedPreferences sharedpreferences = c1.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(token, value);
        editor.apply();
    }

    public static String getUserid(Context c1) {
        SharedPreferences sharedpreferences = c1.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String ans =sharedpreferences.getString(userid,"no");
        return  ans;
    }
    public static void setUserid(Context c1,String value) {
        SharedPreferences sharedpreferences = c1.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(userid, value);
        editor.apply();
    }

    public static String getPromo(Context c1) {
        SharedPreferences sharedpreferences = c1.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String ans =sharedpreferences.getString(promo,"no");
        return  ans;
    }
    public static void setPromo(Context c1,String value) {
        SharedPreferences sharedpreferences = c1.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(promo, value);
        editor.apply();
    }


    public static void clearAll(Context c1)
    {
        SharedPreferences sharedpreferences = c1.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.apply();
    }
    public static void clearValue(Context c1,String name) {
        SharedPreferences sharedpreferences = c1.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.remove(name);
        editor.apply();
    }

    public static String getDevid(Context c1) {
        SharedPreferences sharedpreferences = c1.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String ans =sharedpreferences.getString(devid,"no");
        return  ans;
    }
    public static void setDevid(Context c1,String value) {
        SharedPreferences sharedpreferences = c1.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(devid, value);
        editor.apply();
    }
    public static String getFirstinstall(Context c1) {
        SharedPreferences sharedpreferences = c1.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String ans =sharedpreferences.getString(firstinstall,"no");
        return  ans;
    }
    public static void setFirstinstall(Context c1,String value) {
        SharedPreferences sharedpreferences = c1.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(firstinstall, value);
        editor.apply();
    }

    public static String getFBid(Context c1) {
        SharedPreferences sharedpreferences = c1.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String ans =sharedpreferences.getString(firebaseid,"no");
        return  ans;
    }
    public static void setFBid(Context c1,String value) {
        SharedPreferences sharedpreferences = c1.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(firebaseid, value);
        editor.apply();
    }

}