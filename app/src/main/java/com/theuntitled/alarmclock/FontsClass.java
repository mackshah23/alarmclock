package com.theuntitled.alarmclock;


import android.app.Application;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class FontsClass extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/QuicksandRegular.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
