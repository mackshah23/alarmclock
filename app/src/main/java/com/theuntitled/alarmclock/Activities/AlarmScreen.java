package com.theuntitled.alarmclock.Activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.gelitenight.waveview.library.WaveView;
import com.skyfishjy.library.RippleBackground;
import com.theuntitled.alarmclock.Database.Databaseconnection;
import com.theuntitled.alarmclock.R;
import com.theuntitled.alarmclock.Services.AlarmReceiver;
import com.theuntitled.alarmclock.Utils.WaveHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class AlarmScreen extends NEWAppCompactActivity {
    //Button b;
    private SensorManager mSensorManager;
    private Sensor mProximity;
    private static final int SENSOR_SENSITIVITY = 4;
    MediaPlayer player;
    Uri uri;
    MediaPlayer ringtone;
    AudioManager m_amAudioManager;
    int maxcount = 2;
    Bundle bundle;
    Databaseconnection obj;
    ArrayList<String> dta = new ArrayList<>();
    String ringto, voc = "";
    TextView msg;
    Animation myAnimation;
    String[] cont;
    String name, numbr;
    TextView nm, num;
    //AdView adView;
    private WaveHelper mWaveHelper;
    private int mBorderColor = Color.parseColor("#44FFFFFF");
    private int mBorderWidth = 0;
    WaveView waveView;
    TextView timetext;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        final Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        setContentView(R.layout.alramscreen);
        msg = (TextView) findViewById(R.id.msg);
        nm = (TextView) findViewById(R.id.name);
        num = (TextView) findViewById(R.id.contactnum);
        timetext = findViewById(R.id.alarmtime);
        //adView = (AdView) findViewById(R.id.ad_view);

        //setad(adView);
        myAnimation = AnimationUtils.loadAnimation(this, R.anim.textanimation);
        myAnimation.setRepeatCount(Animation.INFINITE);
        myAnimation.setRepeatMode(Animation.REVERSE);
        msg.startAnimation(myAnimation);
        waveView = (WaveView) findViewById(R.id.wave);
        waveView.setBorder(mBorderWidth, mBorderColor);
        waveView.setShapeType(WaveView.ShapeType.SQUARE);
        mWaveHelper = new WaveHelper(waveView);
        RippleBackground rippleBackground=(RippleBackground)findViewById(R.id.content);

        rippleBackground.startRippleAnimation();
        obj = new Databaseconnection();
        bundle = getIntent().getExtras();
        final String req = bundle.getString("reqcode");
        Log.d("XXXalarm", bundle + "");
        dta = obj.alarmsongs(AlarmScreen.this, req);
        if (dta.size() > 0) {
            ringto = dta.get(0);
            voc = dta.get(1);
            Log.d("XXX", req + " == " + ringto + " == " + voc);
            cont = dta.get(2).split(":");
            nm.setText(cont[0]);
            num.setText(cont[1]);
            timetext.setText(dta.get(3));
        }
        try {
            ringtone = MediaPlayer.create(AlarmScreen.this, Uri.parse(ringto));
            ringtone.start();
        } catch (Exception e) {
            e.printStackTrace();
            Uri uri = Settings.System.DEFAULT_RINGTONE_URI;
            ringtone = MediaPlayer.create(AlarmScreen.this, uri);
            ringtone.start();
        }

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mProximity = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        m_amAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        //b = (Button) findViewById(R.id.dismiss);
        msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (msg.getText().toString().equalsIgnoreCase("Pick Up")) {

                    msg.setText("End");

                    try {
                        Intent intentstop = new Intent(AlarmScreen.this, AlarmReceiver.class);
                        PendingIntent senderstop = PendingIntent.getBroadcast(AlarmScreen.this, 0, intentstop, PendingIntent.FLAG_UPDATE_CURRENT);
                        AlarmManager alarmManagerstop = (AlarmManager) getSystemService(ALARM_SERVICE);
                        alarmManagerstop.cancel(senderstop);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    // uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
                    // ringtone =  MediaPlayer.create(AlarmScreen.this, Settings.System.DEFAULT_RINGTONE_URI);
                    try {
                        ringtone.stop();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    player = new MediaPlayer();

                    if (!voc.equals("")) {
                        try {
                            player.setDataSource(AlarmScreen.this, Uri.parse(voc));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            player.setDataSource(AlarmScreen.this, Settings.System.DEFAULT_RINGTONE_URI);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }


                    player.setAudioStreamType(AudioManager.STREAM_VOICE_CALL);
                    setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
                    m_amAudioManager.setMode(AudioManager.MODE_IN_CALL);
                    m_amAudioManager.setSpeakerphoneOn(false);

                    try {
                        player.prepare();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    try {
                        player.start();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            if (maxcount < 2) {
                                maxcount++;
                                player.seekTo(0);
                                try {

                                    player.start();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                player.stop();
                                Toast.makeText(AlarmScreen.this, "Snooze in 5 minutes.", Toast.LENGTH_LONG).show();
                                Calendar now = Calendar.getInstance();
                                now.add(Calendar.MINUTE, 5);
                                Intent intent = new Intent(AlarmScreen.this, this.getClass());
                                PendingIntent pendingIntent = PendingIntent.getService(AlarmScreen.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                                try {
                                    AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                                    alarmManager.set(AlarmManager.RTC_WAKEUP, now.getTimeInMillis(), pendingIntent);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                } else if (msg.getText().toString().equalsIgnoreCase("End")) {
                    obj.turnoffalarm(AlarmScreen.this, req);
                    msg.setText("Pick Up");
                    try {
                        ringtone.stop();
                        player.stop();
                        player.release();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    finish();
                }
            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();
        mWaveHelper.start();
        if (ad != null) {
            ad.resume();
        }
        //mSensorManager.registerListener(this, mProximity, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        if (ad != null) {
            ad.pause();
        }
        mWaveHelper.cancel();
        super.onPause();
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        Ringtone ringtone = RingtoneManager.getRingtone(AlarmScreen.this, uri);
        try {
            ringtone.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //mSensorManager.unregisterListener(this);
    }


    @Override
    protected void onDestroy() {
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        Ringtone ringtone = RingtoneManager.getRingtone(AlarmScreen.this, uri);
        try {

            ringtone.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (ad != null) {
            ad.destroy();
        }
        super.onDestroy();

    }

    /*@Override
    /*public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_PROXIMITY) {
            if (event.values[0] >= -SENSOR_SENSITIVITY && event.values[0] <= SENSOR_SENSITIVITY) {
                //near
                MediaPlayer player = MediaPlayer.create(this, Settings.System.DEFAULT_RINGTONE_URI);
                player.setAudioStreamType(AudioManager.STREAM_VOICE_CALL);
                player.start();
            } else {
                //far
                MediaPlayer player = MediaPlayer.create(this, Settings.System.DEFAULT_RINGTONE_URI);
                player.setAudioStreamType(AudioManager.STREAM_MUSIC);
                player.start();

            }
        } else {
            MediaPlayer player = MediaPlayer.create(this, Settings.System.DEFAULT_RINGTONE_URI);
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            player.start();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }*/
}
