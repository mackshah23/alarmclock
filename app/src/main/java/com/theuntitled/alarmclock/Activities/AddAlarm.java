package com.theuntitled.alarmclock.Activities;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.kevalpatel.ringtonepicker.RingtonePickerDialog;
import com.theuntitled.alarmclock.Database.Databaseconnection;
import com.theuntitled.alarmclock.R;
import com.theuntitled.alarmclock.Services.AlarmReceiver;
import com.theuntitled.alarmclock.Utils.sharedpreference;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Fizzz on 5/19/2017.
 */

public class AddAlarm extends NEWAppCompactActivity implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {
    LinearLayout l2, l3, l4, l6;
    LinearLayout l1;
    //CardView l7;
    //TextView t7;
    TextView t1, t2, t4, t5, t6;
    AutoCompleteTextView t3;
    Button fb1;
    EditText etname,etnumb;
    Dialog dialogname;
    Button nameok, namecancel;
    EditText alarmnm;
    String almname = "";
    ImageView tnt;
    Dialog dialogrepeat;
    Button repeatok, repeatcancel;
    String mon = "", tue = "", wed = "", thu = "", fri = "", sat = "", sun = "";
    CheckBox a, b, c, d, e, f, g;
    ScrollView sv;
    Databaseconnection obj;
    Date currentTime;
    Uri uriAlarm;
    Ringtone ringTone;
    final int RQS_RINGTONEPICKER = 1;
    final int RQS_MUSICPICKER = 2;
    RingtonePickerDialog.Builder ringtonePickerBuilder = new RingtonePickerDialog.Builder(AddAlarm.this, getSupportFragmentManager());
    String rington = "", musicpth = "";
    AlarmManager alarmManager;
    private PendingIntent pendingIntent;

    String ringtonPath = "na", musicpath = "na";
    String ringtonDefault = "Not Set", musicDefault = "Not Set";
    public static int request_code = 0;
    ArrayList<String> contacts = new ArrayList<String>();
    //ImageView ii1, ii2;
    AdView adView;
    int i = 0;
    InterstitialAd iad;
    private int adcount=0;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        /*requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        setContentView(R.layout.addalarm);

        obj = new Databaseconnection();
        obj.createdatabase(AddAlarm.this);
        request_code = sharedpreference.getReqCode(this);
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent myIntent = new Intent(AddAlarm.this, AlarmReceiver.class);
        myIntent.putExtra("code", request_code + "");
        pendingIntent = PendingIntent.getBroadcast(AddAlarm.this, request_code, myIntent, 0);
        bindcontrols();
        clickevents();
        adView = (AdView)findViewById(R.id.ad_view);
        setad(adView);
        InterstitialAd_config();

    }




    /*private void getContacts() {
        Cursor c = getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                null, null, null);
        while (c.moveToNext()) {

            String contactName = c
                    .getString(c
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phNumber = c
                    .getString(c
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            contacts.add(contactName + ":" + phNumber);

        }
        c.close();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this, android.R.layout.select_dialog_item, contacts);
        t3.setAdapter(adapter);
    }*/

    public void bindcontrols() {
        l1 = (LinearLayout) findViewById(R.id.datelayout);
        l2 = (LinearLayout) findViewById(R.id.timelayout);
        sv = (ScrollView) findViewById(R.id.scrollv);
        l4 = (LinearLayout) findViewById(R.id.ringtonelayout);
        //l5 = (CardView)findViewById(R.id.Snoozelayout);
        l6 = (LinearLayout) findViewById(R.id.voicelayout);
        //l7 = (CardView) findViewById(R.id.repeatlayout);
        t1 = (TextView) findViewById(R.id.selectdate);
        t2 = (TextView) findViewById(R.id.selecttime);
        etname = findViewById(R.id.selectname);
        etnumb = findViewById(R.id.selectnumb);
        t4 = (TextView) findViewById(R.id.selectringtone);
        //t5 = (TextView)findViewById(R.id.selectsnooze);
        t6 = (TextView) findViewById(R.id.selectvoice);
        //t7 = (TextView) findViewById(R.id.selectrepeat);
        fb1 = (Button) findViewById(R.id.savealarm);
        tnt = findViewById(R.id.tint);
        sv = (ScrollView) findViewById(R.id.scrollv);
        //ii1 = (ImageView) findViewById(R.id.color1);
        //ii2 = (ImageView)findViewById(R.id.color2);
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

        try {
            String ccurrentTime = dateFormat.format((new Date()));
            String[] tme = ccurrentTime.split(":");

            if (Integer.parseInt(tme[0])> 0 && Integer.parseInt(tme[0]) < 6 ){
                sv.setBackground(ContextCompat.getDrawable(AddAlarm.this,R.drawable.night));
            } else if (Integer.parseInt(tme[0])> 6 && Integer.parseInt(tme[0]) < 16 ){
                sv.setBackground(ContextCompat.getDrawable(AddAlarm.this,R.drawable.morning));
            } else if (Integer.parseInt(tme[0])> 16 && Integer.parseInt(tme[0]) < 24 ){
                sv.setBackground(ContextCompat.getDrawable(AddAlarm.this,R.drawable.sunset));
            } else {
                sv.setBackground(ContextCompat.getDrawable(AddAlarm.this,R.drawable.sunset));
            }

        } catch (Exception e1) {
            e1.printStackTrace();
        }



        /*ValueAnimator animator = ValueAnimator.ofFloat(0f, 1f);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                ii2.setAlpha((Float) animation.getAnimatedValue());
            }
        });

        animator.setDuration(1500);
        animator.setRepeatMode(ValueAnimator.REVERSE);
        animator.setRepeatCount(-1);
        animator.start();*/


        Calendar c = Calendar.getInstance();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String nowdate = sdf.format(c.getTime());
        t1.setText(nowdate);


        SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm aa");
        String nowtime = sdf1.format(c.getTime());
        t2.setText(nowtime.replace("a.m.", "AM").replace("p.m.", "PM"));

        ArrayList<String> data = new ArrayList<>();
        Uri urii = Settings.System.DEFAULT_RINGTONE_URI;
        Ringtone ringTone = RingtoneManager.getRingtone(getApplicationContext(), urii);
        rington = urii+"";
        musicpth = urii+"";
        ringtonDefault = ringTone.getTitle(this);
        musicDefault = ringTone.getTitle(this);
        data = obj.getdefaults(AddAlarm.this);
        if (data.size() > 0) {
            t4.setText(data.get(1));
            t6.setText(data.get(2));
            ringtonDefault = data.get(1);
            musicDefault = data.get(2);
            rington = data.get(3);
            musicpth = data.get(4);
        } else {
            Uri uri = Settings.System.DEFAULT_RINGTONE_URI;
            ringTone = RingtoneManager.getRingtone(getApplicationContext(), uri);
            t4.setText(ringTone.getTitle(AddAlarm.this));
            ringtonPath = uri.toString();

            t6.setText(ringTone.getTitle(AddAlarm.this));
            musicpath = uri.toString();
        }
        //getContacts();
    }

    public void clickevents() {


        t1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        AddAlarm.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        t2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                TimePickerDialog tpd = TimePickerDialog.newInstance(
                        AddAlarm.this, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), false);
                tpd.show(getFragmentManager(), "Timepickerdialog");
            }
        });

        l4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                startActivityForResult(intent, RQS_RINGTONEPICKER);

                uriAlarm = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                ringTone = RingtoneManager.getRingtone(getApplicationContext(), uriAlarm);
            }
        });


        l6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RQS_MUSICPICKER);
            }
        });



        fb1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (etname.getText().toString().equals("")) {
                    etname.setError("Mandatory field");
                    etname.requestFocus();
                } else if (etnumb.getText().toString().equals("")) {
                    etnumb.setError("Mandatory field");
                    etnumb.requestFocus();
                } else {
                    Date dat = new Date();
                    String[] seldate = t1.getText().toString().split("/");
                    String[] tme = t2.getText().toString().split(":");
                    String a1 = tme[0];
                    String b1 = tme[1];
                    String[] secs = b1.split(" ");
                    String sc = secs[0];
                    Calendar cal = Calendar.getInstance();
                    Log.d("XXX", Integer.parseInt(seldate[0]) + " " + Integer.parseInt(seldate[1]) + " " + Integer.parseInt(seldate[2]) + " " + Integer.parseInt(a1) + " " + Integer.parseInt(sc) + " " + secs[1]);
                    //cal.setTime(dat);
                    cal.set(Calendar.DATE, Integer.parseInt(seldate[0]));
                    cal.set(Calendar.MONTH, Integer.parseInt(seldate[1]) - 1);
                    cal.set(Calendar.YEAR, Integer.parseInt(seldate[2]));

                    if (secs[1].equalsIgnoreCase("AM")) {

                        int a = Integer.parseInt(a1);
                        if (a == 12) {
                            a = 0;
                        }
                        Log.d("XXX1", "" + a);
                        cal.set(Calendar.HOUR_OF_DAY, a);
                    } else {
                        int b = Integer.parseInt(a1);
                        int c;
                        if (b == 12) {
                            c = 12;
                        } else {
                            c = b + 12;
                        }
                        Log.d("XXX2", "" + c);
                        cal.set(Calendar.HOUR_OF_DAY, c);
                    }
                    cal.set(Calendar.MINUTE, Integer.parseInt(sc));
                    cal.set(Calendar.SECOND, 0);
                    cal.set(Calendar.MILLISECOND, 0);

                    String rpt = mon + "," + tue + "," + wed + "," + thu + "," + fri + "," + sat + "," + sun;


                    if (!ringtonPath.equals("na")) {
                        rington = ringtonPath;
                    }

                    if (!musicpath.equals("na")) {
                        musicpth = musicpath;
                    }
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);


                    show_interstitial_ad();
                    obj.insertallalarmdetails(AddAlarm.this, etname.getText().toString()+":"+etnumb.getText().toString(), t1.getText().toString(), t2.getText().toString(), rington, "", musicpth, rpt, "on", request_code);
                    obj.updatevals(AddAlarm.this, ringtonDefault, musicDefault,rington,musicpth);
                    Log.d("AAA", ringtonDefault + " " + musicDefault);
                    Log.d("SSSreq", request_code + "");
                    request_code++;
                    sharedpreference.setReqCode(AddAlarm.this, request_code);
                    finish();
                }
            }
        });
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String dayy = dayOfMonth < 10 ? "0"+dayOfMonth : ""+dayOfMonth;
        String monthh = (monthOfYear+1)< 10 ? "0"+(monthOfYear+1): ""+(monthOfYear+1);
        t1.setText(dayy + "/" + (monthh) + "/" + year);
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
        String minuteString = minute < 10 ? "0" + minute : "" + minute;
        String ampm = "";
        if (hourOfDay > 12) {
            hourOfDay -= 12;
            ampm = "PM";
        } else if (hourOfDay == 12) {
            ampm = "PM";
        } else if (hourOfDay == 0) {
            hourOfDay += 12;
            ampm = "AM";
        } else {

            ampm = "AM";
        }

        String mins = "";
        if (minute < 10) {
            mins = "0" + minute;
        } else {
            mins = String.valueOf(minute);
        }

        t2.setText(hourOfDay + ":" + mins + " " + ampm);
    }

    public void show_interstitial_ad()
    {
        if (adcount == 0) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (iad.isLoaded()) {
                        iad.show();
                    }
                }
            }, 300);
        }
    }
    private void InterstitialAd_config()
    {
        iad = new InterstitialAd(AddAlarm.this);
        iad.setAdUnitId(getString(R.string.interstitialid));
        requestNewInterstitial();
        iad.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });


    }

    private void requestNewInterstitial() {
        String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceId = md5(android_id).toUpperCase();

        if(getString(R.string.adtype).equals("TEST")) {
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice(deviceId)
                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    .build();

            iad.loadAd(adRequest);
        }
        else if(getString(R.string.adtype).equals("ON"))
        {
            AdRequest adRequest = new AdRequest.Builder()
                    .build();

            iad.loadAd(adRequest);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ad != null) {
            ad.resume();
        }
        TimePickerDialog tpd = (TimePickerDialog) getFragmentManager().findFragmentByTag("Timepickerdialog");
        if (tpd != null) tpd.setOnTimeSetListener(this);
        DatePickerDialog dpd = (DatePickerDialog) getFragmentManager().findFragmentByTag("Datepickerdialog");
        if (dpd != null) dpd.setOnDateSetListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RQS_RINGTONEPICKER && resultCode == RESULT_OK) {
            try {
                Uri uri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
                ringTone = RingtoneManager.getRingtone(getApplicationContext(), uri);
                t4.setText(ringTone.getTitle(AddAlarm.this));
                ringtonDefault = ringTone.getTitle(AddAlarm.this);
                ringtonPath = uri.toString();
                if (ringTone.isPlaying()){
                    ringTone.stop();
                }
//                ringTone.play();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == RQS_MUSICPICKER && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            musicpath = String.valueOf(new File(getRealPathFromURI(uri)));

        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        int col_index = -1;
        /*String[] proj = { MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Artists.ARTIST };*/
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Audio.Media.DATA);
            result = cursor.getString(idx);
            col_index = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            t6.setText(cursor.getString(col_index));
            musicDefault = cursor.getString(col_index);
            cursor.close();
        }
        return result;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}/*if (!mon.equals("")) {
                    Calendar cal1 = Calendar.getInstance();
                    cal1.setTime(dat);
                    cal1.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
                    cal1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(a1));
                    cal1.set(Calendar.MINUTE, Integer.parseInt(sc));
                    cal1.set(Calendar.SECOND, 0);
                    cal1.set(Calendar.MILLISECOND, 0);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, cal1.getTimeInMillis(), pendingIntent);
                }
                if (!tue.equals("")) {
                    Calendar cal1 = Calendar.getInstance();
                    cal1.setTime(dat);
                    cal1.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
                    cal1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(a1));
                    cal1.set(Calendar.MINUTE, Integer.parseInt(sc));
                    cal1.set(Calendar.SECOND, 0);
                    cal1.set(Calendar.MILLISECOND, 0);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, cal1.getTimeInMillis(), pendingIntent);
                }
                if (!wed.equals("")) {
                    Calendar cal1 = Calendar.getInstance();
                    cal1.setTime(dat);
                    cal1.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
                    cal1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(a1));
                    cal1.set(Calendar.MINUTE, Integer.parseInt(sc));
                    cal1.set(Calendar.SECOND, 0);
                    cal1.set(Calendar.MILLISECOND, 0);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, cal1.getTimeInMillis(), pendingIntent);
                }
                if (!thu.equals("")) {
                    Calendar cal1 = Calendar.getInstance();
                    cal1.setTime(dat);
                    cal1.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY);
                    cal1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(a1));
                    cal1.set(Calendar.MINUTE, Integer.parseInt(sc));
                    cal1.set(Calendar.SECOND, 0);
                    cal1.set(Calendar.MILLISECOND, 0);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, cal1.getTimeInMillis(), pendingIntent);
                }
                if (!fri.equals("")) {
                    Calendar cal1 = Calendar.getInstance();
                    cal1.setTime(dat);
                    cal1.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
                    cal1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(a1));
                    cal1.set(Calendar.MINUTE, Integer.parseInt(sc));
                    cal1.set(Calendar.SECOND, 0);
                    cal1.set(Calendar.MILLISECOND, 0);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, cal1.getTimeInMillis(), pendingIntent);
                }
                if (!sat.equals("")) {
                    Calendar cal1 = Calendar.getInstance();
                    cal1.setTime(dat);
                    cal1.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
                    cal1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(a1));
                    cal1.set(Calendar.MINUTE, Integer.parseInt(sc));
                    cal1.set(Calendar.SECOND, 0);
                    cal1.set(Calendar.MILLISECOND, 0);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, cal1.getTimeInMillis(), pendingIntent);
                }
                if (!sun.equals("")) {
                    Calendar cal1 = Calendar.getInstance();
                    cal1.setTime(dat);
                    cal1.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                    cal1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(a1));
                    cal1.set(Calendar.MINUTE, Integer.parseInt(sc));
                    cal1.set(Calendar.SECOND, 0);
                    cal1.set(Calendar.MILLISECOND, 0);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, cal1.getTimeInMillis(), pendingIntent);
                }*/
/*l7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogrepeat = new Dialog(AddAlarm.this);
                dialogrepeat.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogrepeat.setContentView(R.layout.repeatdays);
                dialogrepeat.setCanceledOnTouchOutside(false);
                a = (CheckBox) dialogrepeat.findViewById(R.id.checkBox);
                b = (CheckBox) dialogrepeat.findViewById(R.id.checkBox2);
                c = (CheckBox) dialogrepeat.findViewById(R.id.checkBox3);
                d = (CheckBox) dialogrepeat.findViewById(R.id.checkBox4);
                e = (CheckBox) dialogrepeat.findViewById(R.id.checkBox5);
                f = (CheckBox) dialogrepeat.findViewById(R.id.checkBox6);
                g = (CheckBox) dialogrepeat.findViewById(R.id.checkBox7);

                repeatok = (Button) dialogrepeat.findViewById(R.id.repeatok);
                repeatcancel = (Button) dialogrepeat.findViewById(R.id.repeatcancel);

                repeatok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (a.isChecked()) {
                            mon = "Monday";
                        }
                        if (b.isChecked()) {
                            tue = "Tuesday";
                        }
                        if (c.isChecked()) {
                            wed = "Wednesday";
                        }
                        if (d.isChecked()) {
                            thu = "Thursday";
                        }
                        if (e.isChecked()) {
                            fri = "Friday";
                        }
                        if (f.isChecked()) {
                            sat = "Saturday";
                        }
                        if (g.isChecked()) {
                            sun = "Sunday";
                        }
                        dialogrepeat.dismiss();
                    }
                });

                repeatcancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogrepeat.dismiss();
                    }
                });

                dialogrepeat.show();
            }
        });*/