package com.theuntitled.alarmclock.Activities;

import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.theuntitled.alarmclock.R;

import java.security.MessageDigest;

public class NEWAppCompactActivity extends AppCompatActivity {

    AdView ad;
    public void setad( AdView ad)
    {
        this.ad=ad;
        showad();
    }
    public static final String md5(final String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
    public  void showad()
    {
        if(getString(R.string.adtype).equals("TEST"))
        {

            String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            String deviceId = md5(android_id).toUpperCase();
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    .addTestDevice(deviceId)
                    .build();
            ad.loadAd(adRequest);

        }
        else if(getString(R.string.adtype).equals("ON"))
        {

            AdRequest adRequest = new AdRequest.Builder()
                    .build();
            ad.loadAd(adRequest);

        }

        else if(getString(R.string.adtype).equals("OFF"))
        {


            ad.setVisibility(View.GONE);

        }

    }



}

