package com.theuntitled.alarmclock.Activities;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.ads.AdView;
import com.kevalpatel.ringtonepicker.RingtonePickerDialog;
import com.theuntitled.alarmclock.Adapters.Adaptermainactivity;
import com.theuntitled.alarmclock.Database.Databaseconnection;
import com.theuntitled.alarmclock.R;
import com.theuntitled.alarmclock.Services.AlarmReceiver;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Fizzz on 5/26/2017.
 */

public class EditAlarm extends NEWAppCompactActivity implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {
    LinearLayout l2, l4, l6, l7;
    TextView t1, t2, t4, t6, t7;
    Button fb1;
    EditText etname, etnumb;
    //    EditText e1;
    Dialog dialogrepeat;
    Button repeatok, repeatcancel;
    String mon, tue, wed, thu, fri, sat, sun = "";
    CheckBox a, b, c, d, e, f, g;
    //AutoCompleteTextView t3;
    Databaseconnection obj;
    String rington = "", musicpth = "";
    Uri uriAlarm;
    Ringtone ringTone;
    final int RQS_RINGTONEPICKER = 1;
    final int RQS_MUSICPICKER = 2;
    RingtonePickerDialog.Builder ringtonePickerBuilder = new RingtonePickerDialog.Builder(EditAlarm.this, getSupportFragmentManager());
    ArrayList<String> reqcodes = new ArrayList<>();
    AlarmManager alarmManager;
    private PendingIntent pendingIntent;
    LinearLayout l1;
    ArrayList<String> data = new ArrayList<>();
    String ringtonPath = "na", musicpath = "na";
    public static int request_code = 10000;
    ArrayList<String> contacts = new ArrayList<String>();
    //ImageView ii1,ii2;
    AdView adView;
    ScrollView sv;
    ImageView tnt;
    ImageView dlt;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        /*requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        setContentView(R.layout.editalarm);
        //getSupportActionBar().setTitle(Html.fromHtml("<font color='#FFFFFF'>  Edit Alarm </font>"));
        obj = new Databaseconnection();
        obj.createdatabase(EditAlarm.this);
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        bindcontrols();
        clickevents();

        adView = (AdView) findViewById(R.id.ad_view);
        setad(adView);
    }

    public void setdata() {
        data = obj.alarmdetails(EditAlarm.this, Adaptermainactivity.selection);
        if (data.size() > 0) {
            String[] conts = data.get(1).split(":");
            etname.setText(conts[0]);
            etnumb.setText(conts[1]);
            //t3.dismissDropDown();
            t1.setText(data.get(2));
            t2.setText(data.get(3));

            Uri uri = Uri.parse(data.get(4));
            Ringtone ringTone = RingtoneManager.getRingtone(getApplicationContext(), uri);
            t4.setText(ringTone.getTitle(EditAlarm.this));
            rington = uri + "";

            Uri uri1 = Uri.parse(data.get(6));
            Ringtone ringTone1 = RingtoneManager.getRingtone(getApplicationContext(), uri1);
            t6.setText(ringTone1.getTitle(EditAlarm.this));
            musicpth = uri1 + "";
            Log.d("XXX", rington + " 11 " + musicpth);
            //t7.setText(data.get(7));
            reqcodes.add(data.get(9));
            Intent myIntent = new Intent(EditAlarm.this, AlarmReceiver.class);
            myIntent.putExtra("code", data.get(9));
            Log.d("XXXreq", data.get(9));
            pendingIntent = PendingIntent.getBroadcast(EditAlarm.this, Integer.parseInt(data.get(9)), myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        }
    }

    /*private void getContacts() {
        Cursor c = getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                null, null, null);
        while (c.moveToNext()) {

            String contactName = c
                    .getString(c
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phNumber = c
                    .getString(c
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            contacts.add(contactName + ":" + phNumber);

        }
        c.close();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.select_dialog_item,contacts);
        t3.setAdapter(adapter);

    }
*/
    public void bindcontrols() {
        l1 = findViewById(R.id.datelayout1);
        l2 = (LinearLayout) findViewById(R.id.timelayout1);
//        e1 = findViewById(R.id.selectname1);
        l4 = (LinearLayout) findViewById(R.id.ringtonelayout1);
        // l5 = (CardView)findViewById(R.id.Snoozelayout1);
        l6 = (LinearLayout) findViewById(R.id.voicelayout1);
//        l7 = findViewById(R.id.repeatlayout1);
        t1 = (TextView) findViewById(R.id.selectdate1);
        t2 = (TextView) findViewById(R.id.selecttime1);
        etname = findViewById(R.id.selectname);
        etnumb = findViewById(R.id.selectnumb);
        t4 = (TextView) findViewById(R.id.selectringtone1);
        //t5 = (TextView)findViewById(R.id.selectsnooze1);
        t6 = (TextView) findViewById(R.id.selectvoice1);
//        t7 = findViewById(R.id.selectrepeat1);
        fb1 = (Button) findViewById(R.id.savealarm1);
        //t3 = (AutoCompleteTextView) findViewById(R.id.selectname1);
        /*t3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                t3.setText("");
            }
        });*/
        /*ii1 = (ImageView) findViewById(R.id.color1);
        ii2 = (ImageView)findViewById(R.id.color2);*/
        tnt = findViewById(R.id.tint);
        dlt = findViewById(R.id.deletealarm);
        sv = (ScrollView) findViewById(R.id.scrollv);
        //ii1 = (ImageView) findViewById(R.id.color1);
        //ii2 = (ImageView)findViewById(R.id.color2);
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

        try {
            String ccurrentTime = dateFormat.format((new Date()));
            String[] tme = ccurrentTime.split(":");

            if (Integer.parseInt(tme[0]) > 0 && Integer.parseInt(tme[0]) < 6) {
                sv.setBackground(ContextCompat.getDrawable(EditAlarm.this, R.drawable.night));
            } else if (Integer.parseInt(tme[0]) > 6 && Integer.parseInt(tme[0]) < 16) {
                sv.setBackground(ContextCompat.getDrawable(EditAlarm.this, R.drawable.morning));
            } else if (Integer.parseInt(tme[0]) > 16 && Integer.parseInt(tme[0]) < 24) {
                sv.setBackground(ContextCompat.getDrawable(EditAlarm.this, R.drawable.sunset));
            } else {
                sv.setBackground(ContextCompat.getDrawable(EditAlarm.this, R.drawable.sunset));
            }

        } catch (Exception e1) {
            e1.printStackTrace();
        }
        /*ValueAnimator animator = ValueAnimator.ofFloat(0f, 1f);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                ii2.setAlpha((Float) animation.getAnimatedValue());
            }
        });

        animator.setDuration(1500);
        animator.setRepeatMode(ValueAnimator.REVERSE);
        animator.setRepeatCount(-1);
        animator.start();*/
        setdata();
//        getContacts();

    }

    public void clickevents() {


        t1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        EditAlarm.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        t2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                TimePickerDialog tpd = TimePickerDialog.newInstance(
                        EditAlarm.this,
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE),
                        false
                );
                tpd.show(getFragmentManager(), "Timepickerdialog");
            }
        });

        l4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                startActivityForResult(intent, RQS_RINGTONEPICKER);

                uriAlarm = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                ringTone = RingtoneManager.getRingtone(getApplicationContext(), uriAlarm);
            }
        });


        l6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RQS_MUSICPICKER);
            }
        });

        dlt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(EditAlarm.this, android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(EditAlarm.this);
                }
                builder.setTitle("Delete Alarm")
                        .setMessage("Are you sure you want to delete this entry?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                obj.deletedata(EditAlarm.this, Adaptermainactivity.selection);
                                EditAlarm.this.finish();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });

        /*l7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                l7.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogrepeat = new Dialog(EditAlarm.this);
                        dialogrepeat.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialogrepeat.setContentView(R.layout.repeatdays);
                        dialogrepeat.setCanceledOnTouchOutside(false);
                        a = (CheckBox) dialogrepeat.findViewById(R.id.checkBox);
                        b = (CheckBox) dialogrepeat.findViewById(R.id.checkBox2);
                        c = (CheckBox) dialogrepeat.findViewById(R.id.checkBox3);
                        d = (CheckBox) dialogrepeat.findViewById(R.id.checkBox4);
                        e = (CheckBox) dialogrepeat.findViewById(R.id.checkBox5);
                        f = (CheckBox) dialogrepeat.findViewById(R.id.checkBox6);
                        g = (CheckBox) dialogrepeat.findViewById(R.id.checkBox7);

                        repeatok = (Button)dialogrepeat.findViewById(R.id.repeatok);
                        repeatcancel = (Button)dialogrepeat.findViewById(R.id.repeatcancel);

                        repeatok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                if (a.isChecked()){
                                    mon = "Monday";
                                }
                                if (b.isChecked()){
                                    tue = "Tuesday";
                                }
                                if (c.isChecked()){
                                    wed = "Wednesday";
                                }
                                if (d.isChecked()){
                                    thu = "Thursday";
                                }
                                if (e.isChecked()){
                                    fri = "Friday";
                                }
                                if (f.isChecked()){
                                    sat = "Saturday";
                                }
                                if (g.isChecked()){
                                    sun = "Sunday";
                                }
                                dialogrepeat.dismiss();
                            }
                        });

                        repeatcancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialogrepeat.dismiss();
                            }
                        });

                        dialogrepeat.show();
                    }
                });
            }
        });*/

        fb1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etname.getText().toString().equals("")) {
                    etname.setError("Mandatory field");
                    etname.requestFocus();
                } else if (etnumb.getText().toString().equals("")) {
                    etnumb.setError("Mandatory field");
                    etnumb.requestFocus();
                } else {
                    Date dat = new Date();
                    String[] seldate = t1.getText().toString().split("/");
                    String[] tme = t2.getText().toString().split(":");
                    String a1 = tme[0];
                    String b1 = tme[1];
                    String[] secs = b1.split(" ");
                    String sc = secs[0];
                    Calendar cal = Calendar.getInstance();
                    Log.d("XXX", Integer.parseInt(seldate[0]) + " " + Integer.parseInt(seldate[1]) + " " + Integer.parseInt(seldate[2]) + " " + Integer.parseInt(a1) + " " + Integer.parseInt(sc) + " " + secs[1]);
                    //cal.setTime(dat);
                    cal.set(Calendar.DATE, Integer.parseInt(seldate[0]));
                    cal.set(Calendar.MONTH, Integer.parseInt(seldate[1]) - 1);
                    cal.set(Calendar.YEAR, Integer.parseInt(seldate[2]));


                    if (secs[1].equalsIgnoreCase("AM")) {

                        int a = Integer.parseInt(a1);
                        if (a == 12) {
                            a = 0;
                        }
                        Log.d("XXX1", "" + a);
                        cal.set(Calendar.HOUR_OF_DAY, a);
                    } else {
                        int b = Integer.parseInt(a1);
                        int c;
                        if (b == 12) {
                            c = 12;
                        } else {
                            c = b + 12;
                        }

                        cal.set(Calendar.HOUR_OF_DAY, c);
                    }
                    cal.set(Calendar.MINUTE, Integer.parseInt(sc));
                    cal.set(Calendar.SECOND, 0);
                    cal.set(Calendar.MILLISECOND, 0);

                    String rpt = mon + "," + tue + "," + wed + "," + thu + "," + fri + "," + sat + "," + sun;


                    if (!ringtonPath.equals("na")) {
                        rington = ringtonPath;
                    }

                    if (!musicpath.equals("na")) {
                        musicpth = musicpath;
                    }
                    Log.d("XXX2", "" + rington + " == " + musicpth + " == " + ringtonPath + " == " + musicpath);
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
                    obj.updateallalarmdetails(EditAlarm.this, Adaptermainactivity.selection, etname.getText().toString() + ":" + etnumb.getText().toString(), t1.getText().toString(), t2.getText().toString(), rington, musicpth, rpt, "on");


                    finish();
                }
            }
        });
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String dayy = dayOfMonth < 10 ? "0" + dayOfMonth : "" + dayOfMonth;
        String monthh = (monthOfYear + 1) < 10 ? "0" + (monthOfYear + 1) : "" + (monthOfYear + 1);
        t1.setText(dayy + "/" + (monthh) + "/" + year);
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
        String minuteString = minute < 10 ? "0" + minute : "" + minute;
        String secondString = second < 10 ? "0" + second : "" + second;

        String ampm = "";
        if (hourOfDay > 12) {
            hourOfDay -= 12;
            ampm = "PM";
        } else if (hourOfDay == 12) {
            ampm = "PM";
        } else if (hourOfDay == 0) {
            hourOfDay += 12;
            ampm = "AM";
        } else {

            ampm = "AM";
        }

        String mins = "";
        if (minute < 10) {
            mins = "0" + minute;
        } else {
            mins = String.valueOf(minute);
        }

        t2.setText(hourOfDay + ":" + mins + " " + ampm);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ad != null) {
            ad.resume();
        }
        TimePickerDialog tpd = (TimePickerDialog) getFragmentManager().findFragmentByTag("Timepickerdialog");
        if (tpd != null) tpd.setOnTimeSetListener(EditAlarm.this);
        DatePickerDialog dpd = (DatePickerDialog) getFragmentManager().findFragmentByTag("Datepickerdialog");
        if (dpd != null) dpd.setOnDateSetListener(EditAlarm.this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RQS_RINGTONEPICKER && resultCode == RESULT_OK) {
            try {
                Uri uri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
                ringTone = RingtoneManager.getRingtone(getApplicationContext(), uri);
                t4.setText(ringTone.getTitle(EditAlarm.this));
                ringtonPath = uri.toString();
                if (ringTone.isPlaying()) {
                    ringTone.stop();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == RQS_MUSICPICKER && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            musicpath = String.valueOf(new File(getRealPathFromURI(uri)));
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Audio.Media.DATA);
            result = cursor.getString(idx);
            int col_index = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            t6.setText(cursor.getString(col_index));
            cursor.close();
        }
        return result;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
 /*Date dat = new Date();
                String[] seldate = t1.getText().toString().split("/");
                String[] tme = t2.getText().toString().split(":");
                String a1 = tme[0];
                String b1 = tme[1];
                String[] secs = b1.split(" ");
                String sc = secs[0];
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.DATE,Integer.parseInt(seldate[0]));
                cal.set(Calendar.MONTH,Integer.parseInt(seldate[1])-1);
                cal.set(Calendar.YEAR,Integer.parseInt(seldate[2]));
                if (secs[1].equalsIgnoreCase("AM")) {

                    int a = Integer.parseInt(a1);
                    if (a==12){
                        a=0;
                    }
                    Log.d("XXX1",""+a);
                    cal.set(Calendar.HOUR_OF_DAY, a);
                } else {
                    int b = Integer.parseInt(a1);
                    int c;
                    if (b==12){
                        c = 12;
                    }else {
                        c = b + 12;
                    }
                    Log.d("XXX2",""+c);
                    cal.set(Calendar.HOUR_OF_DAY, c);
                }
                cal.set(Calendar.MINUTE, Integer.parseInt(sc));
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND,0);

                alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis() , pendingIntent);*/

                /*if (!a.equals("")){
                    Calendar cal1 = Calendar.getInstance();
                    cal1.setTime(dat);
                    cal1.set(Calendar.DAY_OF_WEEK,Calendar.MONDAY);
                    cal1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(a1));
                    cal1.set(Calendar.MINUTE, Integer.parseInt(sc));
                    cal1.set(Calendar.SECOND, 0);
                    cal1.set(Calendar.MILLISECOND,0);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, cal1.getTimeInMillis(), pendingIntent);
                }
                if (!b.equals("")){
                    Calendar cal1 = Calendar.getInstance();
                    cal1.setTime(dat);
                    cal1.set(Calendar.DAY_OF_WEEK,Calendar.TUESDAY);
                    cal1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(a1));
                    cal1.set(Calendar.MINUTE, Integer.parseInt(sc));
                    cal1.set(Calendar.SECOND, 0);
                    cal1.set(Calendar.MILLISECOND,0);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, cal1.getTimeInMillis(), pendingIntent);
                }
                if (!c.equals("")){
                    Calendar cal1 = Calendar.getInstance();
                    cal1.setTime(dat);
                    cal1.set(Calendar.DAY_OF_WEEK,Calendar.WEDNESDAY);
                    cal1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(a1));
                    cal1.set(Calendar.MINUTE, Integer.parseInt(sc));
                    cal1.set(Calendar.SECOND, 0);
                    cal1.set(Calendar.MILLISECOND,0);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, cal1.getTimeInMillis(), pendingIntent);
                }
                if (!d.equals("")){
                    Calendar cal1 = Calendar.getInstance();
                    cal1.setTime(dat);
                    cal1.set(Calendar.DAY_OF_WEEK,Calendar.THURSDAY);
                    cal1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(a1));
                    cal1.set(Calendar.MINUTE, Integer.parseInt(sc));
                    cal1.set(Calendar.SECOND, 0);
                    cal1.set(Calendar.MILLISECOND,0);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, cal1.getTimeInMillis(), pendingIntent);
                }
                if (!e.equals("")){
                    Calendar cal1 = Calendar.getInstance();
                    cal1.setTime(dat);
                    cal1.set(Calendar.DAY_OF_WEEK,Calendar.FRIDAY);
                    cal1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(a1));
                    cal1.set(Calendar.MINUTE, Integer.parseInt(sc));
                    cal1.set(Calendar.SECOND, 0);
                    cal1.set(Calendar.MILLISECOND,0);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, cal1.getTimeInMillis(), pendingIntent);
                }
                if (!f.equals("")){
                    Calendar cal1 = Calendar.getInstance();
                    cal1.setTime(dat);
                    cal1.set(Calendar.DAY_OF_WEEK,Calendar.SATURDAY);
                    cal1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(a1));
                    cal1.set(Calendar.MINUTE, Integer.parseInt(sc));
                    cal1.set(Calendar.SECOND, 0);
                    cal1.set(Calendar.MILLISECOND,0);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, cal1.getTimeInMillis(), pendingIntent);
                }
                if (!g.equals("")){
                    Calendar cal1 = Calendar.getInstance();
                    cal1.setTime(dat);
                    cal1.set(Calendar.DAY_OF_WEEK,Calendar.SUNDAY);
                    cal1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(a1));
                    cal1.set(Calendar.MINUTE, Integer.parseInt(sc));
                    cal1.set(Calendar.SECOND, 0);
                    cal1.set(Calendar.MILLISECOND,0);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, cal1.getTimeInMillis(), pendingIntent);
                }*/