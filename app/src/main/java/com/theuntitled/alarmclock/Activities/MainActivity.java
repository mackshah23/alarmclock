package com.theuntitled.alarmclock.Activities;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.dekoservidoni.omfm.OneMoreFabMenu;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.theuntitled.alarmclock.Adapters.Adaptermainactivity;
import com.theuntitled.alarmclock.Database.Databaseconnection;
import com.theuntitled.alarmclock.R;
import com.theuntitled.alarmclock.Utils.sharedpreference;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends NEWAppCompactActivity implements OneMoreFabMenu.OptionsClick {

    GridView lv;
    //FloatingActionButton fb;
    Databaseconnection obj;
    ArrayList<String> ids = new ArrayList<>();
    ArrayList<String> nms = new ArrayList<>();
    ArrayList<String> dts = new ArrayList<>();
    ArrayList<String> tms = new ArrayList<>();
    ArrayList<String> sts = new ArrayList<>();
    ArrayList<String> data = new ArrayList<>();
    Adaptermainactivity adaptermainactivity;
    ArrayList<String> reqcodes = new ArrayList<>();
    TextView txt_noalarm;
    public static final int PERMISSION_ALL = 200;
    String[] PERMISSIONS = { Manifest.permission.WAKE_LOCK,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.INTERNET, Manifest.permission.MODIFY_AUDIO_SETTINGS, Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.INTERNET};
    AdView adView;
    InterstitialAd iad;
    private int adcount=0;
    OneMoreFabMenu oneMoreFabMenu;
    //OptionsFabLayout fabWithOptions;
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        obj = new Databaseconnection();
        obj.createdatabase(MainActivity.this);
        lv = (GridView)findViewById(R.id.alarmlists);
        //fb = (FloatingActionButton)findViewById(R.id.addalarm);
        txt_noalarm = (TextView) findViewById(R.id.txt_noalarm);
        adView = (AdView) findViewById(R.id.ad_view);
        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS,PERMISSION_ALL);
        }
        oneMoreFabMenu = findViewById(R.id.fab);

        oneMoreFabMenu.setOptionsClick(MainActivity.this);


        setad(adView);
        InterstitialAd_config();

        adView.setAdListener(new AdListener(){

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                adView.setVisibility(View.GONE);
            }


            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                adView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdClicked() {
                super.onAdClicked();
            }

            @Override
            public void onAdImpression() {
                super.onAdImpression();
            }
        });



        if (sharedpreference.getFirstinstall(this).equalsIgnoreCase("no")){
            Uri uri = Settings.System.DEFAULT_RINGTONE_URI;
            Ringtone ringTone = RingtoneManager.getRingtone(getApplicationContext(), uri);
            obj.insertvals(this,ringTone.getTitle(this),ringTone.getTitle(this),uri+"",uri+"");
            sharedpreference.setReqCode(this,1000);
        }

        sharedpreference.setFirstinstall(MainActivity.this,"yes");

        //fabWithOptions = (OptionsFabLayout) findViewById(R.id.fab_l);

        //Set mini fab's colors.
        /*fabWithOptions.setMiniFabsColors(
                R.color.colorPrimary,
                R.color.green_fab);*/



        data = obj.alarmfewdetails(MainActivity.this);
        if (data.size()>0){
            for (int i = 0;i<data.size();i=i+6){
                ids.add(data.get(i));
                nms.add(data.get(i+1));
                dts.add(data.get(i+2));
                tms.add(data.get(i+3));
                sts.add(data.get(i+4));
                reqcodes.add(data.get(i+5));
            }
            adaptermainactivity = new Adaptermainactivity(MainActivity.this,ids,nms,dts,tms,sts,reqcodes);
            lv.setAdapter(adaptermainactivity);
            txt_noalarm.setVisibility(View.GONE);
            lv.setVisibility(View.VISIBLE);
        } else {
            txt_noalarm.setVisibility(View.VISIBLE);
            lv.setVisibility(View.GONE);
        }
        show_interstitial_ad();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.floatingbuttons, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //Closes menu if its opened.
        /*if (fabWithOptions.isOptionsMenuOpened())
            fabWithOptions.closeOptionsMenu();
        else
            super.onBackPressed();*/

    }

    public void show_interstitial_ad()
    {
        if (adcount == 0) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (iad.isLoaded()) {
                        iad.show();
                    }
                }
            }, 300);
        }
    }
    private void InterstitialAd_config()
    {
        iad = new InterstitialAd(MainActivity.this);
        iad.setAdUnitId(getString(R.string.interstitialid));
        requestNewInterstitial();
        iad.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });


    }

    private void requestNewInterstitial() {
        String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceId = md5(android_id).toUpperCase();

        if(getString(R.string.adtype).equals("TEST")) {
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice(deviceId)
                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    .build();

            iad.loadAd(adRequest);
        }
        else if(getString(R.string.adtype).equals("ON"))
        {
            AdRequest adRequest = new AdRequest.Builder()
                    .build();

            iad.loadAd(adRequest);

        }
    }
    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        data.clear();
        ids.clear();
        nms.clear();
        dts.clear();
        tms.clear();
        sts.clear();
        data = obj.alarmfewdetails(MainActivity.this);
        if (data.size()>0){
            for (int i = 0;i<data.size();i=i+6){
                ids.add(data.get(i));
                nms.add(data.get(i+1));
                dts.add(data.get(i+2));
                tms.add(data.get(i+3));
                sts.add(data.get(i+4));
                reqcodes.add(data.get(i+5));
                txt_noalarm.setVisibility(View.GONE);
                lv.setVisibility(View.VISIBLE);
            }
            adaptermainactivity = new Adaptermainactivity(MainActivity.this,ids,nms,dts,tms,sts,reqcodes);
            lv.setAdapter(adaptermainactivity);
        }else {
            txt_noalarm.setVisibility(View.VISIBLE);
            lv.setVisibility(View.GONE);
        }
    }

    @Override
    public void onOptionClick(Integer integer) {

        switch (integer){
            case R.id.fab_add:
                startActivity(new Intent(MainActivity.this,AddAlarm.class));
                break;

            case R.id.fab_contact:
                String os = System.getProperty("os.version"); // OS version
                int version = Build.VERSION.SDK_INT;          // API Level
                String device = android.os.Build.DEVICE;      // Device
                String model = android.os.Build.MODEL;        // Model
                String product = android.os.Build.PRODUCT;    // Product

                final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                emailIntent.setType("text/plain");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"theuntitleddevelopers@gmail.com"});
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Enquiry From WakeMeFi");
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Device Information:\nOs:" + os + "\nVersion:" + version + "\nDevice Model:" + model + "\n"+device +"\n\nMessage:");
                emailIntent.setType("message/rfc822");

                try {
                    startActivity(Intent.createChooser(emailIntent,"Send email using..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(MainActivity.this,"No email clients installed.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.fab_share:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "With this alarm app, I don't have to ask others to wake me up. Download and see it yourself.\nDownload WakeMeFi - The ultimate alarm app\nhttps://goo.gl/5QDtmU";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Share WakeMeFi - The ultimate alarm app");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                break;
            case R.id.fab_rate:
                Uri uri = Uri.parse("market://details?id=com.theuntitled.alarmclock&hl=en");
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.theuntitled.alarmclock&hl=en")));
                }
                break;
        }
    }
}
